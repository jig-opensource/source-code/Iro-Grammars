# mindcode README

The Game [Mindustry] (https://store.steampowered.com/app/1127400/Mindustry) offers a Scripting Logic which is somewhat similar to assembler, which was popular 1/4 century ago. Therefore, it is very annoying.

That is why it is ingenious that François Beausoleil created the very useful compiler named `Mindcode`:

* [His product presentation](https://www.reddit.com/r/Mindustry/comments/m60qli/mindcode_a_higher_level_language_that_compiles/)
* [Synax](https://github.com/francois/mindcode/blob/main/SYNTAX.markdown)
* [Examples and the online Compiler](https://mindcode.herokuapp.com)


# Installation
Copy it to:
c:\Users\<user>\AppData\Local\Programs\Microsoft VS Code\resources\app\extensions\mindcode\

## Features

- Mindcode Grammar / Syntax for the Mindustry Game Language Compiler
- The Default Dark+ Mindcode which adds some additional Syntax Highlightings


## Requirements
.

## Extension Settings
.

## Known Issues

The syntax/grammar definition is designed more openly than mindcode, because it could be that mindcode will still be extended and I want to have little effort to extend the syntax/grammar later.

Therefore, it can happen that the editor does not show an error, but later the compiler does.

## Release Notes

See: CHANGELOG.md

